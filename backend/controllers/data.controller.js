const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');
const fs = require('fs');
const csv = require('csvtojson');
const async = require('async');
const path = require("path");
const fetch = require('node-fetch');
let promises = [];
const readline = require('readline');

const CompanyList = mongoose.model('CompanyList');
const BSECompanyData = mongoose.model('BSECompanyData');
const NSECompanyData = mongoose.model('NSECompanyData');
const ISINCompanyData = mongoose.model('ISINCompanyData');
const ActivityData = mongoose.model('ActivityData');

const csvFilesPath = ['./../backend/data/BSE-list-of-companies.csv', './../backend/data/NSE-list-of-companies.csv'];

module.exports.getAllData = (req, res, next) => {
    try {
        const readFile = function (file) {
            return new Promise(function (resolve, reject) {
                let lines = [];
                let rl = readline.createInterface({
                    input: fs.createReadStream('./../backend/data/' + file)
                });

                rl.on('line', function (line) {
                    // Split line on comma and remove quotes
                    let columns = line
                        .replace(/"/g, '')
                        .split(',');

                    lines.push(columns);
                });

                rl.on('close', function () {
                    // Add newlines to lines
                    lines = lines.join("\n");
                    resolve(lines)
                });
            });
        };
        const writeFile = function (data) {
            return new Promise(function (resolve, reject) {
                fs.appendFile('results.csv', data, 'utf8', function (err) {
                    if (err) {
                        reject('Writing file error!');
                    } else {
                        resolve('Writing file succeeded!');
                    }
                });
            });
        };
        fs.readdir('./../backend/data/', function (err, files) {
            for (let i = 0; i < files.length; i++) {
                promises.push(readFile(files[i]));

                if (i == (files.length - 1)) {
                    let results = Promise.all(promises);

                    results.then(writeFile)
                        .then(function (data) {
                            res.send('writing file finish');
                        }).catch(function (err) {
                        console.log(err)
                    });
                }

            }
        });
    } catch (err) {
        console.log(err);
    }
};

module.exports.getCompanyDetails = (req, res, next) => {
    try {
        CompanyList.find({},
            (err, CompanyList) => {
                if (!CompanyList)
                    return res.status(404).json({status: false, message: 'Data record not found.'});
                else {
                    let tempArr = CompanyList.map(doc => doc['EnitityName']);
                    return res.status(200).json({
                        status: true, data: tempArr
                    });
                }
            });
    } catch (err) {
        console.log(err);
    }
};

module.exports.getAutoCompanyDetails = (req, res, next) => {
    try {
        BSECompanyData.find({'SecurityName': {"$regex": req.query.text, "$options": "i"}},
            (err, docs) => {
                if (!docs)
                    return res.status(404).json({status: false, message: 'Data record not found.'});
                else {
                    let tempArr = docs.map(doc => {
                        return {EnitityName: doc['SecurityName']}
                    });
                    res.send(tempArr);
                }
            })
    } catch (err) {
        console.log(err);
    }
};

module.exports.findCompanyData = (req, res, next) => {
    try {
        BSECompanyData.find({'SecurityName': {"$regex": req.query.text, "$options": "i"}},
            (err, BSECompanyData) => {
                if (!BSECompanyData)
                    return res.status(404).json({status: false, message: 'Data record not found.'});
                else {
                    if (BSECompanyData.length !== 0) {
                        const BSEISINID = BSECompanyData[0]._doc.ISINNo;
                        const BSEData = BSECompanyData[0]._doc;
                        let findalData;
                        let ISINCompanyDta = {};
                        let ActivityDta = {};

                        NSECompanyData.find({'ISINNo': {"$regex": BSEISINID, "$options": "i"}},
                            (err, NSECompanyData) => {
                                if (!NSECompanyData)
                                    return res.status(404).json({status: false, message: 'Data record not found.'});
                                else {
                                    if (NSECompanyData.length !== 0) {
                                    }
                                }
                            });

                        ISINCompanyData.find({'ISINNo': {"$regex": BSEISINID, "$options": "i"}},
                            (err, ISINCompanyData) => {
                                if (!ISINCompanyData)
                                    return res.status(404).json({status: false, message: 'Data record not found.'});
                                else {
                                    if (ISINCompanyData.length !== 0) {
                                        ISINCompanyDta = ISINCompanyData[0]._doc;
                                        findalData = Object.assign({}, ISINCompanyDta, BSEData);
                                        // res.send([findalData]);

                                        ActivityData.find({'ISINNo': {"$regex": BSEISINID, "$options": "i"}},
                                            (err, ActivityData) => {
                                                if (!ActivityData)
                                                    return res.status(404).json({status: false, message: 'Data record not found.'});
                                                else {
                                                    if (ActivityData.length !== 0) {
                                                        ActivityDta = ActivityData[0]._doc;
                                                        findalData = Object.assign({}, findalData, ActivityDta);
                                                        res.send([findalData]);
                                                    } else {
                                                        res.send([findalData]);
                                                    }
                                                }
                                            });
                                    } else {
                                        res.send(BSECompanyData)
                                    }
                                }
                            });
                    }
                }
            })
    } catch (err) {
        console.log(err);
    }
};