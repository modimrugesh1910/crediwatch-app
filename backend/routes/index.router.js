const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const ctrlData = require('../controllers/data.controller');

const jwtHelper = require('../config/jwtHelper');

router.post('/register', ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile',jwtHelper.verifyJwtToken, ctrlUser.userProfile);
router.get('/data', ctrlData.getAllData);
router.get('/companylist', ctrlData.getCompanyDetails);
router.get('/company', ctrlData.findCompanyData);
router.get('/autocompany', ctrlData.getAutoCompanyDetails);
module.exports = router;



