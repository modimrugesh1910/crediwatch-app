const mongoose = require('mongoose');
const _ = require('lodash');

const ActivitySchema = new mongoose.Schema({
    SYMBOL: {
        type: String,
        required: 'SYMBOL can\'t be empty'
    },SERIES: {
        type: String,
        required: 'SERIES can\'t be empty'
    },OPEN: {
        type: Number,
        required: 'OPEN can\'t be empty'
    },HIGH: {
        type: Number,
        required: 'HIGH can\'t be empty'
    },LOW: {
        type: Number,
        required: 'LOW can\'t be empty'
    },CLOSE: {
        type: Number,
        required: 'CLOSE can\'t be empty'
    },PREVCLOSE: {
        type: Number,
        required: 'PREVCLOSE can\'t be empty'
    },TOTTRDQTY: {
        type: Number,
        required: 'TOTTRDQTY can\'t be empty'
    },TOTTRDVAL: {
        type: Number,
        required: 'TOTTRDVAL can\'t be empty'
    },TOTALTRADES: {
        type: Number,
        required: 'TOTALTRADES can\'t be empty'
    },ISINNo: {
        type: String,
        required: 'ISINNo can\'t be empty'
    },TRADING_DATE: {
        type: String,
        required: 'TRADING_DATE can\'t be empty'
    }
}, { _id: false, id: false });

mongoose.model('ActivityData', ActivitySchema);