const mongoose = require('mongoose');
const _ = require('lodash');

const ISINCompanyDataSchema = new mongoose.Schema({
    SC_CODE: {
        type: Number,
        required: 'EntityName can\'t be empty'
    },SC_NAME: {
        type: String,
        required: 'SecurityId can\'t be empty'
    },SC_GROUP: {
        type: String,
        required: 'SecurityName can\'t be empty'
    },SC_TYPE: {
        type: String,
        required: 'Status can\'t be empty'
    },OPEN: {
        type: Number,
        required: 'Group can\'t be empty'
    },HIGH: {
        type: Number,
        required: 'FaceValue can\'t be empty'
    },LOW: {
        type: Number,
        required: 'ISINNo can\'t be empty'
    },CLOSE: {
        type: Number,
        required: 'Industry can\'t be empty'
    },PREVCLOSE: {
        type: Number,
        required: 'Instrument can\'t be empty'
    },NO_TRADES: {
        type: Number,
        required: 'Instrument can\'t be empty'
    },NO_OF_SHRS: {
        type: Number,
        required: 'Instrument can\'t be empty'
    },NET_TURNOV: {
        type: Number,
        required: 'Instrument can\'t be empty'
    },TDCLOINDI: {
        type: Number,
        required: 'Instrument can\'t be empty'
    },ISINNo: {
        type: String,
        required: 'Instrument can\'t be empty'
    },TRADING_DATE: {
        type: String,
        required: 'Instrument can\'t be empty'
    }
}, { _id: false, id: false });

mongoose.model('ISINCompanyData', ISINCompanyDataSchema);