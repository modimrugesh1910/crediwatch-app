const mongoose = require('mongoose');
const _ = require('lodash');

const BSECompanyDataSchema = new mongoose.Schema({
    SecurityCode: {
        type: String,
        required: 'EntityName can\'t be empty'
    },SecurityId: {
        type: String,
        required: 'SecurityId can\'t be empty'
    },SecurityName: {
        type: String,
        required: 'SecurityName can\'t be empty'
    },Status: {
        type: String,
        required: 'Status can\'t be empty'
    },Group: {
        type: String,
        required: 'Group can\'t be empty'
    },FaceValue: {
        type: String,
        required: 'FaceValue can\'t be empty'
    },ISINNo: {
        type: String,
        required: 'ISINNo can\'t be empty'
    },Industry: {
        type: String,
        required: 'Industry can\'t be empty'
    },Instrument: {
        type: String,
        required: 'Instrument can\'t be empty'
    }
}, { _id: false, id: false });

mongoose.model('BSECompanyData', BSECompanyDataSchema);