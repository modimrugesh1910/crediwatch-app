const mongoose = require('mongoose');
const _ = require('lodash');

const NSECompanyDataSchema = new mongoose.Schema({
    SYMBOL: {
        type: Number,
        required: 'EntityName can\'t be empty'
    }, NAMEOFCOMPANY: {
        type: String,
        required: 'SecurityId can\'t be empty'
    }, SERIES: {
        type: String,
        required: 'SecurityName can\'t be empty'
    }, DATEOFLISTING: {
        type: String,
        required: 'Status can\'t be empty'
    }, PAIDUPVALUE: {
        type: String,
        required: 'Group can\'t be empty'
    }, MARKETLOT: {
        type: String,
        required: 'FaceValue can\'t be empty'
    }, ISINNo: {
        type: String,
        required: 'ISINNo can\'t be empty'
    }, FACEVALUE: {
        type: String,
        required: 'Industry can\'t be empty'
    }
}, {_id: false, id: false});

mongoose.model('NSECompanyData', NSECompanyDataSchema);