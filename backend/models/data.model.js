const mongoose = require('mongoose');
const _ = require('lodash');

var dataSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: 'id can\'t be empty',
        unique: true
    }, umpire3: {
        type: String,
        required: 'umpire3 can\'t be empty'
    }
});

mongoose.model('Data', dataSchema);