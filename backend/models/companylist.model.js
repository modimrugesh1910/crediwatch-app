const mongoose = require('mongoose');
const _ = require('lodash');

const CompanyListSchema = new mongoose.Schema({
    EntityName: {
        type: String,
        required: 'EntityName can\'t be empty'
    }
}, { _id: false, id: false });

mongoose.model('CompanyList', CompanyListSchema);