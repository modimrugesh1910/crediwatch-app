# IncredibleHAck @Crediwatch

## Team Name 
### TheHackster

## For client side 
### Development Build

1. `npm install`
2. `npm start`

### Production Build

1. `npm run build`

## For Server/API side

1. `npm install`
2. `npm start` or `node app.js`

## For mongo collection import 
please use mongoimport command or Studio 3T to import collections directly into database.
please find tutorial - https://www.quackit.com/mongodb/tutorial/mongodb_import_data.cfm

### contact details  

phone - 7990782930 

## Github
git clone https://modimrugesh1910@bitbucket.org/modimrugesh1910/crediwatch-app.git

## Activities Done - 

I have tried to resolve the first problem - SINGULARITY

I have covered below details -

* make a list of 1000 businesses from a source
* sharing data over files
* map all the data from the sources to a unique identifier.
* make an object of the Business which will link to all the data objects from various sources.
* Build a searchable dashboard of these unique Businesses
* A minimum of 4 source widgets need to be present for every business in the list.
* version control
