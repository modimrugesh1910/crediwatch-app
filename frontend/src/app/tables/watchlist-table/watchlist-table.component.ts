import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {TableService} from '../tables.service';
import {fromEvent as observableFromEvent, BehaviorSubject, Observable} from 'rxjs';
import {distinctUntilChanged, debounceTime, switchMap} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';

@Component({
    selector: 'app-watchlist-table',
    templateUrl: './watchlist-table.component.html',
    styleUrls: ['./watchlist-table.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class WatchlistTableComponent implements OnInit, AfterViewInit {
    showList = false;
    showTable = true;
    companyList: any;

    myControl = new FormControl();
    filteredOptions: Observable<any>;
    companyData: any;

    constructor(private readonly tableService: TableService) {
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {
        this.tableService.fetchCompanyName('').subscribe((res) => {
            this.companyList = res;

            this.filteredOptions = this.myControl.valueChanges
                .pipe(
                    startWith(''),
                    debounceTime(100),
                    map((value: any) => {
                        return this._filter(value);
                    })
                );
        });
    }

    getBackToTable() {
        this.showList = false;
        this.showTable = true;
    }

    getCompanyData(str) {
        console.log(str)
        this.tableService.fetchCompanyData(str).subscribe((data) => {
            this.companyData = data[0];
            console.log(data);
        })
    }

    private _filter(value: string): any {
        if (value !== '') {
            // this.tableService.fetchCompanyName(value).subscribe((data) => {
            //     console.log(data);
            // });
            // this.filteredOptions = this.tableService.fetchCompanyName(value);
            const filterValue = value.toLowerCase();
            return this.companyList.filter(option => option.EnitityName.toLowerCase().includes(filterValue));
        }
    }
}

