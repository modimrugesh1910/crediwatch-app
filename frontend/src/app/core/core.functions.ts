import * as moment from 'moment';
import {FormGroup} from '@angular/forms';

declare const require;
declare const Buffer;

export const convertDate = (date): string => {
    return moment(date).format('DD-MMM-YYYY');
};

// const crypto = require('crypto');
// const algorithm = 'aes-256-cbc';
const algorithm = 'aes-256-ctr';

export const getRawData = (form: FormGroup) => {
    return form.getRawValue();
};

export function getDiffColor(val): string {
    if (val === 0) {
        return '';
    } else if (val > 0) {
        return 'green';
    } else {
        return 'red';
    }
}

export function isString(value: any): value is string {
    return typeof value === 'string';
}

export function createRound(method: string): Function {
    // <any>Math to suppress error
    const func: any = (<any>Math)[method];
    return function (value: number, precision: number = 0) {
        if (typeof value === 'string') {
            throw new TypeError('Rounding method needs a number');
        }

        if (typeof precision !== 'number' || isNaN(precision)) {
            precision = 0;
        }

        if (precision) {
            let pair = `${value}e`.split('e');
            const val = func(`${pair[0]}e` + (+pair[1] + precision));
            pair = `${val}e`.split('e');
            return +(pair[0] + 'e' + (+pair[1] - precision));
        }
        return func(value);
    };
}

export function _guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return (
        s4() +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        s4() +
        s4()
    );
}
