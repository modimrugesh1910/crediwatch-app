const EXPRESS_SERVER = 'http://localhost:8080/';

/** Websocket Server */
export  const ws_server = 'https://determined-dubinsky-3bbcbb.netlify.com/';

/** URL strings used in API calls */
export const URL_API = {
  /**
   * Login api
   */
  LOGIN: EXPRESS_SERVER + 'login',
  LOCAL_STORAGE: {
    KEYS: {
      TOKEN: 'access-token',
      CART_DATA: 'cart-data'
    }
  },
  COMPANY_DATA: function(): string {
    return EXPRESS_SERVER + 'api/companylist';
  },
  COMPANY_AUTO: function (str): string {
    return EXPRESS_SERVER + 'api/autocompany/?text=' + str;
  },
  COMPANY: function (str): string {
    return EXPRESS_SERVER + 'api/company/?text=' + str;
  }
};
